require 'twitter'
require 'pp'
require 'json'

def env(key)
  k = "TWITTER_" + key.upcase
  ENV.fetch(k)
end

client = Twitter::REST::Client.new do |config|
  config.consumer_key        = env('consumer_key')
  config.consumer_secret     = env('consumer_secret')
  config.access_token        = env('consumer_access_token')
  config.access_token_secret = env('consumer_access_token_secret')
end

keywords = ARGV.first
count = ARGV[1].to_i

warn format("searching for '%s', count %d", keywords, count)

File.open('tmp.json', 'w') do |f|
  f.puts('[')
  client.search(keywords, lang: 'en').take(count).each_with_index do |tw, idx|
    f.write JSON.pretty_generate(tw.to_h).to_s

    if idx < count - 1
      f.puts(",\n")
    else
      f.puts("\n")
    end
  end
  f.puts(']')
end
