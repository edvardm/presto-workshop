require 'json'
require 'pp'
require 'date'

require 'cassandra'

class CassandraLoader
  def initialize(connection)
    @connection = connection
  end

  def insert_user(tw)
    uid = tw['user']['id'].to_i
    login = tw['user']['screen_name']
    url = tw['user']['url']
    name = tw['user']['name']
    location = tw['user']['location']
    following = int_or_nil tw['user']['friends_count']
    followers = int_or_nil tw['user']['followers_count']
    created_at = parse_time(tw['user']['created_at'])

    @user_stmt ||= @connection.prepare('insert into users (user_id, login,  url, name, location, following, followers, created_at) values (?, ?, ?, ?, ?, ?, ?, ?)')

    args = { arguments: [uid, login, url, name, location, following, followers, created_at] }
    begin
      @connection.execute(@user_stmt, args)
    rescue => e
      warn "could not insert #{args.inspect}: #{e}"
    end
  end

  def insert_tweet(tw)
    tweet_id = tw['id'].to_i
    uid = tw['user']['id'].to_i
    txt = tw['text']
    reply_to_tweet = int_or_nil tw['in_reply_to_status_id']
    reply_to_user =  int_or_nil tw['in_reply_to_user_id']
    created_at = parse_time(tw['created_at'])

    @tweet_stmt ||= @connection.prepare('insert into tweets (tweet_id, user_id, txt, reply_to_tweet, reply_to_user, created_at) values (?, ?, ?, ?, ?, ?)')
    args = { arguments: [tweet_id, uid, txt, reply_to_tweet, reply_to_user, created_at] }

    begin
      @connection.execute(@tweet_stmt, args)
    rescue => e
      warn "could not insert #{args.inspect}: #{e}"
    end
  end

  private

  def parse_time(s)
    DateTime.parse(s).to_time
  end

  def int_or_nil(v)
    v ? v.to_i : 0
  end
end

keyspace = 'demo'
infile = ARGV.first || fail("usage: loadtweets.rb <jsonfile>")

cluster = Cassandra.cluster
cluster.each_host do |host|
  puts "Host #{host.ip}: id=#{host.id} datacenter=#{host.datacenter} rack=#{host.rack}"
end
sess = cluster.connect(keyspace)

cassandra_loader = CassandraLoader.new(sess)

res = JSON.parse(File.read(infile))
warn "Read #{res.size} twitter entries"

res.each do |tw|
  cassandra_loader.insert_user(tw)
  cassandra_loader.insert_tweet(tw)
end

