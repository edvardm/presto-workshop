# Setting up things 

First download these, if needed:

* cassandra (http://docs.datastax.com/en/cassandra/2.0/cassandra/install/installDeb_t.html, https://wiki.apache.org/cassandra/DebianPackaging)
* presto-server (https://prestodb.io/docs/current/installation/deployment.html)
* presto-cli (https://prestodb.io/docs/current/installation/cli.html)
* postgres (optional)

I'd recommend creating a sandbox directory for these things, eg. 
`~/presto-workshop`

# Configure stuff

Cassandra is usually trivial to install with packaging tools, and default config is fine. Presto needs to be configured though, see etc/ dir
for details on different catalogues and presto server configuration.

When finished, run

`bin/launcher -v run` to launch presto-server

# Creating schema

Run cqlsh (mnemonic: cee-quel-sh) to create demo keyspace and schema:

```
create keyspace demo with replication = { 
  'class' : 'SimpleStrategy', 'replication_factor' : 1 
};

use demo;

create table users (
  user_id bigint primary key,
  login ascii,
  url text,
  name text,
  location text,
  following int,
  followers int,
  created_at timestamp
);
create index on users(created_at);

create table tweets (
  tweet_id bigint primary key,
  user_id bigint,
  txt text,
  reply_to_tweet bigint,
  reply_to_user bigint,
  created_at timestamp
);
```

### Tweets in PostgreSQL (optional)

```
create table tweets (
  tweet_id int8 primary key,
  user_id int8,
  txt text,
  reply_to_tweet int8,
  reply_to_user int8,
  created_at timestamp
);
```

# Loading tweet data to databases

Get sample tweet test data (I did query /w keyword 'haskell') 

`curl -s https://dl.dropboxusercontent.com/u/1222751/tweets.json.gz | gzip -dc tweets.json`

# Installing Ruby (not needed, if `which ruby` finds a ruby binary)

Getting rvm:

\curl -sSL https://get.rvm.io | bash -s stable --ruby

Then saying `rvm install ruby-2.2.1` should do the trick (note: using ruby 2.2.2 is ok too, but 2.2.1 is more simple, likely)

# Running tweet loader

Install Ruby tweet loader deps:

(note: you need Gemfile for bundler to work, see the repo)

`gem install bundler && bundle`

**Note: if not using postgres, loadtweets.rb needs to be modified a bit**

Load tweet data to cassandra / postgres:

`ruby loadtweets.rb tweets.json`

(if using postgres: try to connect to postgres via presto using

`./presto --server localhost:8080 --catalog postgresql --schema public`

Note the syntax: <catalog>.<schema>.table

`select * from postgresql.public.tweets`)

# Toying around in cassandra

`cqlsh > select * from users;`  # ok
`cqlsh > select * from tweets;` # ok
`cqlsh > select users.login, tweets.txt from users, tweets where users.user_id = tweets.user_id;` # not ok - cassandra is not a relational db

`./presto --server localhost:8080 --catalog cassandra --schema demo`
`presto:demo> select users.login, tweets.txt from users, tweets where users.user_id = tweets.user_id;` # works in presto \o/

# Combining multiple databases (from different engines!)

```
./presto --server localhost:8080
presto:default> select u.login, t.txt from cassandra.demo.users u join postgresql.public.tweets t on t.user_id = u.user_id;
```

## Finding out query costs

EXPLAIN / EXPLAIN (TYPE DISTRIBUTED)

## Running more advanced queries with presto

See https://prestodb.io/docs/current/functions.html -- there's loads of nice stuff!